This repository is related to the following  ISBI 2022 submission : TO COMPLETE

The following data are available:
* **expertized lineage** (layer 1, 2 and 3) extracted from [1] and corrected for third layer and border associations.

--> tracking_data/ 

Cell lineage data. text files whose lines are:
mother cell labels in segmented image : daughter cells labels in segmented image at next time point


* **border cells** defined using MorphoNet [2] 

-->  border_cells.csv

Two columns defined the time-point and the label corresponding to the border cells


* **acquisition time converter** : conversion between time-points and acquisition times (in hours)

--> acquisition_time_conversion.txt

text files whose lines are:
time_point acquisition_time_in_hours


***References***:

[1] Y.  Refahi,  A.  Zardilis,  G.  Michelin,  R.  Wightman,B. Leggio, J. Legrand, E. Faure, L. Vachez, A. Armez-zani,  A.-E.  Risson,  et  al.A  multiscale  analysis  ofearly flower development in arabidopsis provides an in-tegrated view of molecular regulation and growth con-trol.Developmental Cell, 56(4):540–556, 2021

[2] Leggio, B., Laussu, J., Carlier, A., Godin, C., Lemaire, P., & Faure, E. et al. MorphoNet: an interactive online morphological browser to explore complex multi-scale data. Nature communications, 10(1), 1-8, 2019
